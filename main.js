///Iteración #1: Buscar el máximo

/*
var numero1 = 3;
var numero2 = 2;

function sum(numberOne, numberTwo){
    if(numberOne > numberTwo){
        console.log(numberOne);
    }else{
        console.log(numberTwo);
    }
}

sum(numero1, numero2);
*/


///Iteración #2: Buscar la palabra más larga


/*
const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];

function findLongestWord(paramOne) {
    let candidato = avengers[0];
    for(let i = 0; i <avengers.length; i++){
        if(avengers[i].length > candidato.length){
            candidato = avengers[i];
        }
    }
    return candidato;
}
let masLargo = findLongestWord(avengers);
console.log(masLargo);
*/

///Iteración #3: Calcular la suma

/*
const numbers = [1, 2, 3, 5, 45, 37, 58];
var total = 0;

function sumNumbers(paramOne) {
    for(let i = 0; i < numbers.length; i++){
        // total = total + numbers[i];
        total+=numbers[i]
    }
    console.log(total)
}
let prueba = sumNumbers(numbers);
*/

///Iteración #4: Calcular el promedio

/*
const numbers = [12, 21, 38, 5, 45, 37, 6];
var suma = 0;
var numOfNumbers= 0;
var promedio = 0;

function average(paramOne) {
    for( let i = 0; i <numbers.length; i++){
        suma+=numbers[i];
        numOfNumbers = numbers.length;
    }
    console.log(suma);
    console.log(numOfNumbers);
    promedio = suma/numOfNumbers;
}
average(numbers);
console.log(`La media de los números es de ${promedio}`);

*/

///Iteración #5: Calcular promedio de strings


/* const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];
var totalNumbers = 0;
var totalNumbersString = 0;
var total = 0;
function averageWord(paramOne) {
    mixedElements.forEach(element => {
        if(typeof element == 'number'){
            totalNumbers += element;
        }else{
            totalNumbersString += element.length;
        }
    });
//    console.log(totalNumbers);
//    console.log(totalNumbersString);
}
averageWord(mixedElements);

total = totalNumbersString + totalNumbers;
console.log(total);
 */
///Iteración #6: Valores únicos
/*
const duplicates = [
    'sushi',
    'pizza',
    'burger',
    'potatoe',
    'pasta',
    'ice-cream',
    'pizza',
    'chicken',
    'onion rings',
    'pasta',
    'soda'
];
var noReplyFood = [];
function removeDuplicates(paramOne) {
    duplicates.forEach(element => {
        if(!noReplyFood.includes(element)){
            noReplyFood.push(element);
        }
    });
    console.log(noReplyFood);
}
removeDuplicates(duplicates);
*/

///Iteración #7: Buscador de nombres


const nameFinder = [
    'Peter',
    'Steve',
    'Tony',
    'Natasha',
    'Clint',
    'Logan',
    'Xabier',
    'Bruce',
    'Peggy',
    'Jessica',
    'Marc'
];


function finderName(paramOne,nombre){
((paramOne.includes(nombre))) ? console.log(`${nombre} está en la lista de invitados`) : console.log(`${nombre} no está en la lista de invitados`);
}
let nombre = 'Carlos';
finderName(nameFinder, nombre);


///Iteración #8: Contador de repeticiones

/* const counterWords = [
    'code',
    'repeat',
    'eat',
    'sleep',
    'code',
    'enjoy',
    'sleep',
    'code',
    'enjoy',
    'upgrade',
    'code'
];

var repetidos = {};

function repeatCounter(paramOne) {
    counterWords.forEach(element => {
        repetidos[element] = (repetidos[element] || 0) +1;
    });
    console.log(repetidos);
}

repeatCounter(counterWords);


*/